# Problem to solve

Software requirements of digital materials vary greatly and collecting all necessary software is a huge task for any institution. Various institutions have or will have copies of software objects that may be of use to others and could be shared to advance access to digital materials. Based on the principles defined by the Code of Best Practices in Fair Use for Software, software objects and emulation environments in which they are configured may be shared between institutions despite existing license restrictions if these resources are used for research, pedagogy, etc.

# Impacted Editions

[X] Hosted
[X] Local

# Analysis

**Requirements**

* The system must publish metadata of software objects for retrieval by nodes in a network
* The system must publish metadata of base environments for retrieval by nodes in a network
* The system must publish contextual/descriptive software application metadata for harvesting by other nodes in an EaaSI network
* The system must retrieve metadata about new resources shared to the network and add it to the node's resource metadata
* The system must prohibit content environments and objects from being shared in an EaaSI network
* The system must prohibit the sharing of private metadata properties (e.g., local identifiers) to the network
* The system must prohibit changes to metadata records of resources shared to a network
* The system could automatically synchronize with network nodes according to scheduled intervals
* The system must enable use of remote software objects from the network in local emulation sessions
* The system must download environment image data to node storage from network nodes upon request
* Environments and software objects copied from the network are immediately accessible by all node users

**Use Cases**
* Admin publishes environment or software object to network
* User copies environment from network
* Admin syncs node with other network locations

**Risks**

| Type | Description |
| ------ | ------ |
| User Behavior | Environment or software object shared erroneously |
| System Design | Local nodes may delete only copy of resource if they decide to delete node and associated storage | 
| Security | Breach of storage locations for hosted network resources |
| User Behavior | Environments with miniscule differences shared to the network making it difficult to determine an appropriate base to use in configuration |

# Dev Approach

**Design Summary**

The EaaSI network makes use of the Open Archives Initiative Protocol for Metadata Harvesting (OAI-PMH) to request, share and synchronize metadata between nodes.

Each EaaSI installation contains an OAI-PMH harvester and a data provider. The harvester requests metadata (in EaaSI’s case, Base Environment and Software Object records) from data providers at other nodes; the data providers query the node’s local records and return this metadata back to the original harvester.

![Publish to network](https://drive.google.com/file/d/1Cpagn6qan8qjRWht4BZ5nemkWdBvnVZT/view?usp=sharing)

![Copy from network](https://drive.google.com/file/d/1gHi5ZbTEB38U6bnfgGeyMHfnlUUCWUSe/view?usp=sharing)

**Dependencies**

**Drawbacks**

**Alternatives**

**Work packages and resource estimates**

- [Local edition] Update interface to support network publish/save for software objects – @portalmedia
- [Hosted edition] Update interface to support network publish for software objects and remove "save" option – @portalmedia
- Implement process for assignment of UUID for software objects

# Open Questions
* **What component is minting unique identifiers for software objects?**
* **How do we include additional software properties (e.g., context, descriptive, system requirements) along with object metadata?** Contingent on implementation of expanded metadata model

# Future Possibilities


