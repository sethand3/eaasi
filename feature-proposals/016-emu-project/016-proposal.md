**Emulation Project - Feature Set Proposal**
-

**Summary**

The emulation project interface provides a central location for the compilation of resources and initial configuration of an environment session. Users may add resources to their emulation project while exploring the contents of the node. Once they've compiled the necessary resources, they select which ones they would like to use in their environment session (e.g., a base environment and an object). With resources selected and UI settings configured, the user will "Run" their environment to start a configuration session.

The Emulation Project workflow also supports creation of "root" environments that are not derived from existing images in the node. The system provides templates of hardware configurations designed for specific computing environments (e.g., Windows 98 PC, Apple System 8 PPC) from which the user will select the most appropriate for their new environment. Users may install an operating system using objects from the library or assign an existing disk image (e.g., content environment) to the boot disk.

**Motivation**
- Stakeholders require uniform mechanisms and workflows for interaction with emulation components

**Business Rules**
- An emulation environment should have an associated hardware template (whether selected or inherited from the base environment)
- An emulation environment may have only one software object or content object selected for environment config
- An emulation project may have any number of resources in its queue/cart

**Risks**

| Type | Description |
| ------ | ------ |
| System Design | Users don't understand best practices for setting up their emulation environment |
| User Behavior | Users select an object that isn't compatible with the selected base environment |
| User Behavior | Users use the emulation project in place of the bookmarking feature |
| User Behavior | Users compile too many resources in the project and are unable to easily find them in their queue |

**Designs** https://xd.adobe.com/view/47926ad4-0c55-4983-7d83-6cbcc5ad6ad3-31a3/

**Status**
-

http://eaasi.portalmedia.com/ updated with initial emulation project features. Requires QA testing and bug fixes.

# Dev Team Response

# Open Questions
