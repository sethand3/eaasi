**Previous discussion of use case in #506**

**Use Case - Create Derivative Base Environment**
-

**Description** Users create new or revise existing base environments by adding software or changing configuration settings.

**Pre-condition** Software object imported into system or available in network; base environment in node or available in network

**Ordinary Sequence**
1. The *user* selects a software object in the Explore Resources interface
2. The *system* displays the resource's action menu
3. The *user* adds the object to their emulation project
4. The *system* adds the software object to their project queue *Requirement #333*
5. The *user* selects a base environment in the Explore Resources interface
6. The *system* displays the resource's action menu
7. The *user* adds the environment to their emulation project
8. The *system* adds the environment to their project queue *Requirement #333*
9. The *user* opens the Emulation Project interface
10. The *system* shows the contents of the user's queue
11. The *user* selects the base environment from their queue
12. The *system* populates the emulation project interface with base environment details - *Requirement #326*
13. The *user* selects the software object from their queue
14. The *system* populates the emulation project interface with software object details
15. The *system* activates the "Run" button active
16. The *user* clicks "Run"

**Post-condition** Environment configuration session is started with software object attached to system drive

>>>
**Error Sequence**

15. The *user* attempts to add another software object or content object to the emulation project config
16. The *system* alerts the user they may only add one software or content object at a time - *Requiement #325*
>>>
