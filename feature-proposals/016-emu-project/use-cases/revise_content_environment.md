**Use Case - Revise Content Environment**
-

**Description**

Users may also revise the configuration of existing content environments to create contrasting versions of environments or make necessary adjustments for operation.

**Pre-condition** Content environment created and available in node or My Resources

**Ordinary Sequence**
1. The *user* selects a content environment in the Explore Resources or My Resources interface
2. The *system* displays the resource's action menu
3. The *user* adds the environment to their emulation project
4. The *system* adds the environment to their project queue - *Requirement #333*
5. The *user* opens the Emulation Project interface 
6. The *system* shows the contents of the user's queue
7. The *user* selects the content environment from their queue
8. The *system* populates the emulation project interface with content environment details - *Requirement #326*
9. The *system* makes the "Run" button active
10. The *user* clicks "Run"

**Post-condition** Environment configuration session started

>>>
**Alternate Sequence**
1. The *user* selects a content environment in the Explore Resources or My Resources interface
2. The *system* displays the resource's action menu
3. The *user* selects "Run in Emulator"
>>>
