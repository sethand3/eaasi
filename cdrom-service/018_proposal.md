**Proposal MR** !18

# Summary

A standalone service or an optional module of the EaaSI emulation software, the CD-ROM Sharing Service allows institutions to re-use content environments containing published CD-ROM titles from Yale's pre-configured collection. From a list of published CD-ROMs in a library’s collection, the service identifies corresponding environments in the Yale collection. An identifier or URL is provided for referencing the environment from local access pages.

# Motivation

* The service is intended to provide institutions access to pre-configured emulation environments featuring broadly distributed publications in the circulating collections of libraries (likely CD-ROMs, but could include other formats in the future).

# Analysis

**High Level Workflow**

The following workflow was the basis for the design:

1. A user uploads a .csv file listing items in their collection that may be available through the service (assume an export of “all CD-ROMs” or something similar
2. The system queries the available inventory of emulation environments to identify which titles from the list are configured in the service
3. The system presents the user with the results of the query, indicating which items have a matched environment
4. User exports a .csv with access links to the pre-configured emulation environments of titles submitted
5. User adds links to their own catalog records or provides discovery of links through some other means
6. End-users discover available titles in collection
7. Access page is generated for end-user interaction
8. The designs also feature some admin screens to manage users (staff who could upload .csv files) and skin the access interface.

**Assumptions**

*  Service is hosted and administered by YUL (e.g., "users" are institutions with individual accounts and don't need a prior EaaSI affiliation)
   *  Requires some kind of admin view for YUL?
*  Users communicate which CD-ROMs they would like to access by uploading a .csv file to the service
*  Users cannot see environments until they have provided the list OR could be provided a pick list to identify their environments
*  CD-ROMs are identified by ISBN(?) or some other uniform identifier
*  Users cannot alter the environments they are provided access to
*  Users should be able to skin their access pages with color and logo of their institution
*  Clients/tenants/whatever must login to reach admin site (to load csv, export, etc.) but will only need one account
*  Access sites will not include descriptive information, it is assumed end-users will access through a catalog or some other metadata record

**Designs**

https://xd.adobe.com/view/59cd17ec-46d7-4412-6db2-903cba6ff945-2ffc/

# Dev Response

**Design Summary**

CD-ROM management is a special case of an multi-tenant object repository. Every tenant has shares all CD-ROMs with 0 licenses. Upload of license file updates licence allowance. Rationale: if license enforcement happens on the object / objec-archive level, it does not matter how many environments exist, i.e. Mac/Win.

**Dependencies**

UI work to be completed. 

**Drawbacks**

**Alternatives**

**Work packages and resource estimates**

- Investigate user-context modelling 
  - based on pre-configured IP ranges
  - using signed requests
  - using OAuth OIC / JWT

- Implement/extend existing user-context 

- Multi-tenant object archive:
https://gitlab.com/openslx/eaas-server/-/issues/77 

- Integrate UI work, prepare deployment (documentation)

# Questions

* How will the system identify the available environments from the lists provided by users? The designs assume something universal like an ISBN but that might limit what titles we provide. Also, this information would need to be stored somewhere and structured for querying. 

*the current data model allows to find an "object" environment by ID if a manual assignment has been done, and object customizations - meaning we could manually assign an ISBN or similar ID to identify the object environments*

* Where is the service layer and other infrastructure for this hosted? Our original assumption was that the CD-ROM configuration work that’s been done for Yale’s service on presemu01 would be the initial inventory for this service, but I’m not sure how this would work. 

*yale-eaasi inc but would we need to scale into the cloud? Could be a greater scale of users than we've previously dealt with if numerous organizations start using*

*Usability and latency concerns would indicate that we need to use some cloud infrastructure for emucomp*

* Related to the previous question, are we hosting the access sites in full as designed (allows for easy skinning)? Or will users be required to build a site like Yale has? How would hosting the access sites impact access permissions we could provide? For example, I assume we wouldn’t be able to support authentication or network restrictions since the site isn’t within the institution’s infrastructure as it is with Yale’s 

*sounds like we could provide an access site as @caseyarendt designed, but need to implement some barrier to ensure users are affiliated or located within the institution. IDP integration would seem to complicate matters immensely. is IP restriction enough of a limitation?*

*Distinguishing IP ranges*

* Not only about security 
* A bit more complicated to enforce accounting 
  * example 
    * ND has one license 
    * We have to be sure this seat is occupied by a ND user 
* IP range -> ISBN -> # of seats  
* Enforced on a different level than access restriction to the service in general 
* Need user context modeling 
  * Would be far easier to do in a second stage 
  * Allow the institutions to host the landing page themselves 
  * would need a web server 
  * Euan – challenging to implement at smaller institutions 
  * Assume that there is almost 0 tech support at client institutions 
* Might not be possible for some institutions to get their IP range 
* Could use subdomain of our own site


# Future Possibilities
*  EaaSI nodes may incorporate the CD-ROM service as an add-on in their instance (not MVP)
*  Not limited to CD-ROM titles and could include items on floppy or other formats
*  Could setup more efficient cloud backend and deploy in multiple regions
